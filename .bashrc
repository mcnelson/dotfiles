# Linux profile. Included in make_symlinks_in_homedir.sh.
# anything ONLY used for Linux should go in here.

source ~/.common_profile

export QMAKE=/usr/bin/qmake-qt4                             # Capybara linux fix
export RUBY_GC_MALLOC_LIMIT=59000000
export RUBY_GC_HEAP_INIT_SLOTS=600000
export RUBY_HEAP_FREE_MIN=100000
export WINEARCH=win64

# This is AFTER caps2esc. 'caps' really refers to escape.
setxkbmap -option caps:none # Unbind capslock (escape)
xmodmap -e 'keycode 66=minus' # Rebind capslock to something useful
# xmodmap -e 'keycode 37=minus' # Rebind control to something useful

# setxkbmap -option caps:Q                               # Caps lock is escape
# setxkbmap -option caps:none
# xmodmap -e 'keycode 37=' # Disable ctrl key
# setxkbmap -option 'caps:ctrl_modifier' && xcape -e 'Caps_Lock=Escape' &

# Disable the fffcking sleep button
#gsettings set org.gnome.settings-daemon.plugins.power button-suspend "nothing"

# Disable Alt+F1
gsettings set org.gnome.desktop.wm.keybindings panel-main-menu "[]"

# Disable stupid PackageKit updates, I update my system manually
gsettings set org.gnome.software download-updates false

alias ls="ls --group-directories-first --color"             # Columnized dir list
alias ll="ls -lh --group-directories-first --color"         # Dir list
alias l="ls -lha --group-directories-first --color"         # Dir list hidden files
alias go="gnome-open"                                       # Gnome-open

# Global stuff
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

source ~/.bashrc.local
