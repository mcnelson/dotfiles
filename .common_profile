#!/bin/sh

# Common profile, used for BOTH Linux & Mac.

stty -ixon                                                  # Ctrl+S
unset command_not_found_handle                              # No delay when cmd isn't found

export LSCOLORS=GxFxCxDxBxegedabagaced                      # Brighter blues against black

export BUNDLER_EDITOR=~/neovim/bin/nvim                     # Make bundle open usable
export EDITOR=~/neovim/bin/nvim                             # Make npm edit usable

# Move cursor to EOL and enter insert mode on commit. TODO causes delay
# export GIT_EDITOR="vim -c'startinsert|norm! ggA '"

export PLATFORMIO_BUILD_DIR=/tmp/platformio_cache

# Locally modified neovim
alias vim="~/neovim/bin/nvim"
alias nvim="~/neovim/bin/nvim"

export PATH="$HOME/.rbenv/bin:$PATH"                        # Rbenv
eval "$(rbenv init -)"

export PATH="./node_modules/.bin:$PATH"

# Use ag with FZF
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'

alias tmux="tmux -2"                                        # 256 colors always in tmux

alias gs='git status'                                       # Git
alias gl='git log'
alias gst='git stage -A .'
alias grt='git reset .'
alias gd='git diff'
alias gds='git diff --staged'
alias gdh='git diff HEAD'
alias gdu='git diff'
alias gg='git grep --untracked -i -n'
alias gc='git commit'
alias gca='git commit --amend'
gch() {
  git checkout $@ && prompt_for_upstream
}
alias gdm="git difftool master..."
alias grh="git reset --hard"
alias grv="git remote -v"
alias gbm="git branch -m"
alias gbd="git branch -D"

# DB quick status tool
dbs() {
  if [[ -n $3 ]]; then
    mysql -e "SHOW INDEXES FROM $1.$2 \G"
  elif [[ -n $2 ]]; then
    mysql -e "SHOW COLUMNS FROM $1.$2"
  elif [[ -n $1 ]]; then
    mysql -B -e "SHOW TABLES FROM $1" |
    less
  else
    mysql -B -e "SHOW SCHEMAS" |
    grep -v "Database\|information_schema
    \|performance_schema\|mysql"
  fi
}

# Git branch last
gbl() {
  [ -z $1 ] && count=30 || count=$1
  git for-each-ref \
    --sort=committerdate refs/heads/ \
    --format='%(refname:short)' | fzf --tac | xargs git checkout

  # select branch in $branch_list; do
  #   git checkout $branch && prompt_for_upstream
  #   break
  # done
}

git_prune_branches() {
  [ -z $1 ] && branch=master || branch=$1

  git branch --merged $branch | \
    grep -v master | \
    grep -v $branch | \
    xargs -n 1 git branch -D
}

# Prompts
parse_git_branch() {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  echo ${ref#refs/heads/}
}

PS1="\[\033[0;36m\]\$(parse_git_branch) \[\033[0;37m\]\$(basename \$PWD | sed 's/\///')/ · "
export MYSQL_PS1="\d ⬞ "
printf '%b' '\e]12;cyan\a'

# http://stackoverflow.com/a/19533853
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
export HISTIGNORE='gs:gl:gst:grt:gd:gds:gdh:gdu:gca:grh'
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="tmuxwindowname; history -a; $PROMPT_COMMAND"

function colortest {
  awk 'BEGIN{
    s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
    for (colnum = 0; colnum<77; colnum++) {
        r = 255-(colnum*255/76);
        g = (colnum*510/76);
        b = (colnum*255/76);
        if (g>255) g = 510-g;
        printf "\033[48;2;%d;%d;%dm", r,g,b;
        printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
        printf "%s\033[0m", substr(s,colnum+1,1);
    }
    printf "\n";
  }'
}

function git-edit-conflicts() {
  vim +/"<<<<<<<" $( git diff --name-only --diff-filter=U | xargs )
}

function git-find-merge() {
  git rev-list $1..master --ancestry-path | grep -f <(git rev-list $1..master --first-parent) | tail -1
}

function tmuxwindowname() {
  if [ -n $TMUX ]; then
    tmux rename-window `basename $PWD`
  fi
}

function prompt_for_upstream() {
  CURRENT_BRANCH=`parse_git_branch`
  test -z "$CURRENT_BRANCH" && return

  REMOTE=`git remote | grep "origin" || git remote | grep -m 1 -E '\w'`

  # Exit 0 => branch has a tracking branch
  # Exit 128 => branch has no tracking branch
  git rev-parse --abbrev-ref $CURRENT_BRANCH@{upstream} &> /dev/null
  HAS_TRACKING_BRANCH=$?

  # Exit 1 => branch name does not exist on remote
  # Exit 0 => branch name exists on remote
  git branch -a | grep $REMOTE/$CURRENT_BRANCH &> /dev/null
  BRANCH_NAME_IS_ON_REMOTE=$?

  if [[ $HAS_TRACKING_BRANCH -ne 0 && $BRANCH_NAME_IS_ON_REMOTE -eq 0 ]]; then
    read -p "Track corresponding remote branch on remote '$REMOTE'? " -n 1 -r
    echo ''

    if [[ $REPLY =~ ^[Yy]$ ]]; then
      git branch --set-upstream-to=$REMOTE/$CURRENT_BRANCH
    fi
  fi
}

# Autoload node version via .nvmrc
cd() {
  builtin cd "$@"
  if [[ -f .nvmrc ]]; then
    nvm use > /dev/null
  fi

  if [[ -f .yvmrc ]]; then
    yvm use > /dev/null
  fi
}
cd .
