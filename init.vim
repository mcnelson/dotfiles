call plug#begin('~/.config/nvim/plugged')
Plug 'junegunn/fzf', { 'dir': '~/.fzf' }                                " Fuzzy file finder
Plug 'junegunn/fzf.vim'                                                 " Find in files live
Plug 'bmcilw1/mustang-vim'                                              " Theme
Plug 'ervandew/supertab'                                                " Async tab completion
Plug 'w0rp/ale'                                                         " Async Lint
Plug 'slim-template/vim-slim'                                           " Slim lang syntax highlighting
Plug 'yaymukund/vim-rabl'
Plug 'ap/vim-css-color'
Plug 'cakebaker/scss-syntax.vim'
Plug 'elixir-editors/vim-elixir'
Plug 'tpope/vim-rails'
Plug 'itchyny/lightline.vim'                                            " Nice statusbar
Plug 'tomtom/tcomment_vim'
Plug 'airblade/vim-gitgutter'
Plug 'bronson/vim-visual-star-search'
Plug 'goldfeld/ctrlr.vim'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'tpope/vim-endwise'
Plug 'benmills/vimux'
Plug 'janko-m/vim-test'
Plug 'tpope/vim-fugitive'
Plug 'posva/vim-vue'
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-rhubarb'
Plug 'kchmck/vim-coffee-script'
Plug 'isRuslan/vim-es6'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'pangloss/vim-javascript'
Plug 'ludovicchabant/vim-gutentags'
Plug 'kristijanhusak/vim-js-file-import', {'do': 'npm install'}
Plug 'puremourning/vimspector'
Plug 'KabbAmine/zeavim.vim'

" " nvim language service
" Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'bash install.sh' }
"
" " nvim language service plugin for typescript
" Plug 'mhartington/nvim-typescript', { 'do': ':UpdateRemotePlugins', 'for': ['typescript', 'typescript.tsx']}
"
" " .ts syntax highlighting
" Plug 'leafgarland/typescript-vim', {'for': ['typescript', 'typescript.tsx']}

call plug#end()

set noswapfile            " Don't use swapfiles
set sel=inclusive         " ?
set sw=2                  " Shiftwidth
set ts=2                  " Tabstop
set ai                    " Autoindent
set nowrap                " No wrapping
set et                    " Expand tab
set ic                    " Ignore case
set nu                    " Line numbers
set ls=2                  " Always display statusbar (laststatus)
set hls                   " Always highlight searches
set is                    " Incremental Search
set ar                    " Autoread
set noow                  " No overwrite warning
set fdm=indent            " Folding
set foldlevel=99          " Unfold all when first opening a file
set splitbelow            " Open splits where you'd expect
set pastetoggle=<F10>     " Toggle paste mode
set isk+=-                " Hyphens act as underscores

inoremap kk <Esc>:w<CR>   " Quick save
nnoremap <C-L> :Ack --ignore ".git/" --ignore "log/" --ignore "vendor/" -i 
map <Leader>s :tabnew<CR>:Ack --ignore ".git/" --ignore "log/" --ignore "vendor/" -i 
map <Leader>og :.Gbrowse<CR>

" JS File Import
let g:js_file_import_filename_filters=['.test.', 'dist\/']
let g:js_file_import_use_fzf = 1
map tt <Plug>(JsGotoDefinition)
map ti <Plug>(JsFileImport)
map ts <Plug>(SortJsFileImport)

" Universal CTags
let g:gutentags_ctags_tagfile = '.git/vim_ctags'
let g:gutentags_add_default_project_roots = 0
let g:gutentags_project_root  = ['package.json', '.git']
let g:gutentags_cache_dir = expand('~/.gutentags_cache')
let g:gutentags_exclude_filetypes = ['gitcommit', 'gitconfig', 'gitrebase', 'gitsendemail', 'git']
let g:gutentags_generate_on_new = 1
let g:gutentags_generate_on_missing = 1
let g:gutentags_generate_on_write = 1
let g:gutentags_generate_on_empty_buffer = 0
let g:gutentags_ctags_extra_args = ['--tag-relative=yes', '--fields=+ailmnS']
let g:gutentags_ctags_exclude = [
\  '*.git', 'cache', 'build', 'dist', 'bin', 'node_modules', 'bower_components',
\  '*-lock.json',  '*.lock',
\  '*.min.*',
\  '*.bak',
\  '*.zip',
\  '*.pyc',
\  '*.class',
\  '*.sln',
\  '*.csproj', '*.csproj.user',
\  '*.tmp',
\  '*.cache',
\  '*.vscode',
\  '*.pdb',
\  '*.exe', '*.dll', '*.bin',
\  '*.mp3', '*.ogg', '*.flac',
\  '*.swp', '*.swo',
\  '.DS_Store', '*.plist',
\  '*.bmp', '*.gif', '*.ico', '*.jpg', '*.png', '*.svg',
\  '*.rar', '*.zip', '*.tar', '*.tar.gz', '*.tar.xz', '*.tar.bz2',
\  '*.pdf', '*.doc', '*.docx', '*.ppt', '*.pptx', '*.xls',
\]

" Supertab
set complete-=t " Do not use typedefs (ctags) for tab complete, it's too slow

" autocmd InsertLeave * silent! '[,']s/\("[^ ]*"\)\(\s*\)=>/\1 => /g | normal! `^

" Fix Vue syntax highlighting
autocmd FileType vue syntax sync fromstart

" Fix .inky syntax highlighting (should be eruby but there is a bug)
autocmd BufNewFile,BufRead *.inky set filetype=html

map <Leader>rh :s/[:"]\([^=,'"]*\)"\?=>/\1: /g<CR>:s/,/,\r/g<CR>

" Theme
colors mustang
hi Search       ctermfg=255 ctermbg=124 guifg=white   guibg=red
hi Folded       ctermfg=255 ctermbg=232 guifg=#a0a8b0 guibg=#384048
hi Visual       ctermfg=15  ctermbg=236 guifg=#faf4c6 guibg=#3c414c

" Overwritten by lightline
"hi StatusLine   ctermfg=14  ctermbg=238  guifg=#a0a8b0 guibg=#384048
"hi StatusLineNC ctermfg=238 ctermbg=15  guifg=#444444 guibg=#444444
hi VertSplit    ctermfg=238 ctermbg=238 guifg=#444444 guibg=#444444 term=reverse cterm=reverse

hi ErrorMsg     ctermfg=15  ctermbg=88  guifg=#a0a8b0 guibg=#384048
hi PreProc      ctermfg=229             guifg=#faf4c6               term=underline
hi Function     ctermfg=15                                          cterm=bold
hi SpellCap     ctermfg=15  ctermbg=236
hi Todo         ctermfg=16 ctermbg=11

hi SignColumn                 ctermfg=white ctermbg=238

" Fuzzy file finder
noremap ff <Esc>:FZF<CR>
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit' }

" Trailing whitespace highlighting
set list
set listchars=tab:→\ ,trail:.

" Shift-klear
noremap <S-K> :noh<CR>:ALEResetBuffer<CR>

" Strip trailing whitespace
command! Stw :%s/\s\+$//

" Copy to sys clipboard
set clipboard=unnamedplus
imap <Leader>y "+y

" Less cluttered folded lines
function! MyFoldText() " {{{
    let line = getline(v:foldstart)

    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount) + 2
    return line . repeat(" ",fillcharcount) . foldedlinecount . ' '
endfunction " }}}
set foldtext=MyFoldText()

" Indent guides
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
let g:indent_guides_default_mapping = 0 " Conflicts with vim-js-file-import
hi IndentGuidesEven guibg=gray ctermbg=235
hi IndentGuidesOdd guibg=gray ctermbg=235

" vim-test
function! VimuxSimpleCommand(cmd)
  call VimuxRunCommand(a:cmd)
endfunction

let g:test#custom_strategies = {'vimux_simple': function('VimuxSimpleCommand')}
let test#strategy = "vimux_simple"
let g:test#preserve_screen = 1
let test#javascript#jest#executable = 'yarn test'
let test#ruby#rspec#executable = 'spring rspec'

noremap Q :w<CR>:TestNearest<CR>
noremap - :w<CR>:TestNearest<CR>
noremap <C-T> :w<CR>:TestNearest<CR>
inoremap <C-T> <Esc>:w<CR>:TestNearest<CR>
map <Leader>rf :w<CR>:TestFile<CR>
map <Leader>rl <Esc>:VimuxRunCommand getline('.')<CR>

" Lightline
let g:lightline = {'component_function': {'filename': 'LightLineFilename'}}
function! LightLineFilename()
  return expand('%')
endfunction

" Ag
if executable('ag')
  let g:ackprg = 'ag --vimgrep --hidden'
endif

" Open Markdown in Github
noremap <silent> <leader>om :call OpenMarkdownPreview()<cr>

function! OpenMarkdownPreview()
  if exists('s:markdown_job_id') && s:markdown_job_id > 0
    call jobstop(s:markdown_job_id)
    unlet s:markdown_job_id
  endif
  let s:markdown_job_id = jobstart(
    \ 'grip ' . shellescape(expand('%:p')) . " 0 2>&1 | awk -F ':|/' '/Running/ { print $5 }'",
    \ { 'on_stdout': function('OnGripStart'), 'pty': 1 })
endfunction

function! OnGripStart(job_id, data, event)
  let port = a:data[0][0:-2]
  call system('google-chrome http://localhost:' . port)
endfunction

" Make Alternate go to request specs from controllers
let g:rails_projections = {
      \  "app/controllers/*_controller.rb": {
      \      "test": [
      \        "spec/requests/{}_controller_spec.rb",
      \        "spec/requests/{}_spec.rb",
      \        "spec/controllers/{}_controller_spec.rb",
      \        "test/controllers/{}_controller_test.rb"
      \      ],
      \      "alternate": [
      \        "spec/requests/{}_controller_spec.rb",
      \        "spec/requests/{}_spec.rb",
      \        "spec/controllers/{}_controller_spec.rb",
      \        "test/controllers/{}_controller_test.rb"
      \      ],
      \   },
      \   "spec/requests/*_spec.rb": {
      \      "command": "request",
      \      "alternate": "app/controllers/{}_controller.rb",
      \      "template": "require 'rails_helper'\n\n" .
      \        "RSpec.describe '{}' do\nend",
      \   },
      \ }
