This is the core of my development environment configuration. I use Fedora primarily, though this supports MacOS too.

- **init.vim** - NeoVim configuration (2018) with awesome Git and Ruby integration, asynchronous linting and tab completion, and refined color scheme / statusbar. Uses vim-plugged (~/.config/nvim/autoload/plug.vim). Just run `:PlugInstall` to get set up. `ff` in normal mode for fuzzy file finder. `kk` in insert mode to save file.
- **.vimrc** - Old Vim configuration (2012ish).
- **.bashrc** - Bash configuration with minimalistic prompt with Git branch name, `ctrl+S` (incremental search), git tools/alises such as `gbl` for a MRU/recent branch picker, no limit on command history, and caps lock remapped to escape key.
- **.bash_profile** - Bash configuration for MacOS. Same thing as above.
- **.tmux.conf** - `Ctrl+Q` leader key, `Ctrl+Q Ctrl+W` for visual mode with yank to system clipboard, `HJKL` keys for pane navigation, true color support, refined look for window tabs, and `Ctrl+Q Shift+Q` to create 3-up pane setup with vim in the right half of the screen.
- **.terminator** - Large, fullscreen, semitransparent, borderless terminal window using Proggy Clean font.
- **.gemrc** - Skips generating documentation, speeding up `bundle install`
- **.dir_colors** - Lightens blue color for directories.
- **.gitconfig** - Change your name! Sets `git push` to work with the current branch.
- **.inputrc** - Allows `Shift+Tab` to autocomplete conflicted file/directory names for bash tab completion.

The idea is to symlink all config from the various places it lives in your home directory to wherever you check out this repo. Run `./make_symlinks_in_homedir.sh` on Linux or `./make_symlinks_in_homedir_mac.sh` on MacOS to make this happen.
