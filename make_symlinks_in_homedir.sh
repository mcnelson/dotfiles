#!/bin/sh

# Symlinks for Linux env

ln -Tnsf $PWD/.vimrc ~/.vimrc
mkdir ~/.config/nvim
ln -Tnsf $PWD/init.vim ~/.config/nvim/init.vim
ln -Tnsf $PWD/.common_profile ~/.common_profile
ln -Tnsf $PWD/.bashrc ~/.bashrc
ln -Tnsf $PWD/.gemrc ~/.gemrc
ln -Tnsf $PWD/.inputrc ~/.inputrc
ln -Tnsf $PWD/.gitconfig ~/.gitconfig
ln -Tnsf $PWD/.gitignore_global ~/.gitignore_global
ln -Tnsf $PWD/.pryrc ~/.pryrc
ln -Tnsf $PWD/.irbrc ~/.irbrc
ln -Tnsf $PWD/.tmux.conf ~/.tmux.conf
ln -Tnsf $PWD/.fonts ~/.fonts
ln -Tnsf $PWD/.dir_colors ~/.dir_colors
ln -Tnsf $PWD/.rubocop.yml ~/.rubocop.yml

mkdir -p ~/.config/terminator
ln -Tnsf $PWD/.terminator ~/.config/terminator/config
