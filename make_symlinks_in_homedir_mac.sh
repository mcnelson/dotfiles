#!/bin/sh
# Symlinks for MacOS env

mkdir ~/.config/nvim
ln -nsf $PWD/init.vim ~/.config/nvim/init.vim
ln -nsf $PWD/.bash_profile ~/.bash_profile
ln -nsf $PWD/.common_profile ~/.common_profile

ln -nsf $PWD/.gemrc ~/.gemrc
ln -nsf $PWD/.gitconfig ~/.gitconfig
ln -nsf $PWD/.gitignore_global ~/.gitignore_global

ln -nsf $PWD/.pryrc ~/.pryrc
ln -nsf $PWD/.irbrc ~/.irbrc
ln -nsf $PWD/.tmux.conf ~/.tmux.conf
ln -nsf $PWD/.rubocop.yml ~/.rubocop.yml
