require 'rails/console/app'
extend Rails::ConsoleMethods
puts 'Rails Console Helpers loaded'

Pry.commands.alias_command 'c', 'continue'
Pry.commands.alias_command 's', 'step'
Pry.commands.alias_command 'n', 'next'
Pry.commands.alias_command 'f', 'finish'
Pry.commands.alias_command 'q', '!!!'
