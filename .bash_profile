# Mac OS profile. Not included in make_symlinks_in_homedir.sh.
# anything ONLY used for Mac OS should go in here.

source ~/.common_profile

alias ls="ls -G"
alias ll="ls -lhG"
alias l="ls -lhaG"
alias go="open"                                             # Mac open

# Make gvim do something on mac
alias gvim="gedit"

source ~/.bashrc.local
